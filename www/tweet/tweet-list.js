define(['text!tweet/tweet-list.html', "../base/openapi", '../base/util', '../base/bigImg/bigImg', '../base/login/login', '../base/caretInsert/jquery.hammer.min'],
	function(viewTemplate, OpenAPI, Util, BigImg, Login) {
		return Piece.View.extend({
			id: 'tweet_tweet-list',
			bigImg: null,
			events: {
				// "click .tweetContent": "goToTweetDetail",
				"click .tweetText": "goToTweetDetail", //在动弹列表的右边，把文字和图片分开，点击文字跳到动弹详情
				"click .listImg": "bigImg", //点击图片查看大图
				"click .issueBtn": "goToIssueTweet",
				"click .atwho": "atwho",
				"click .tweetListImg": "goToUserInfor"

			},
			deleteTweet: function(el) {
				var me = this;
				var dialog = new Piece.Dialog({
					autoshow: false,
					target: 'body',
					title: '确定删除此动弹？',
					content: ''
				}, {
					configs: [{
						title: '确认',
						eventName: 'ok'
					}, {
						title: '取消',
					}],
					ok: function() {
						//检查登陆
						var checkLogin = Util.checkLogin();
						if (checkLogin === false) {
							var login = new Login();
							login.show();
							return;
						}
						var that = me;
						var userToken = Piece.Store.loadObject("user_token");
						var accessToken = userToken.access_token;
						var $target = $(el.currentTarget);
						console.info($target);
						var id = $target.attr("data-id");
						// console.info(id);
						Util.Ajax(OpenAPI.tweet_delete, "GET", {
							access_token: accessToken,
							tweetid: id,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							if (data.error === "200") {
								new Piece.Toast("删除成功");
								// that.onShow();
								Util.reloadPage('tweet/tweet-list?reload=1')
								// that.refreshList();
							} else {
								new Piece.Toast(data.error_description);
							}
						}, null, null);
					}
				});

				dialog.show();
				$('.cube-dialog-screen').click(function() {
					dialog.hide();
				})

			},
			goToUserInfor: function(imgEl) {
				//如果confirm出现，那么return，不进入个人信息。
				if (typeof $('.cube-dialog-screen').get(0) != 'undefined') {
					return;
				};
				Util.imgGoToUserInfor(imgEl);
			},
			bigImg: function(el) {
				Util.bigImg(el);
			},
			atwho: function(el) {
				var me = this;
				var $target = $(el.currentTarget);
				var id = $target.attr("data-ident");
				Util.Ajax(OpenAPI.user_information, "GET", {
					friend: id,
					dataType: 'jsonp'
				}, 'json', function(data, textStatus, jqXHR) {
					var author = data.name;
					author = encodeURI(author);
					var authorid = data.user;
					me.navigateModule("common/common-seeUser?" + "fromAuthor=" + author + "&fromAuthorId=" + authorid, {
						trigger: true
					});
				}, null);

			},
			goToTweetDetail: function(el) {
				//如果confirm出现，那么return，不进入详情。
				if (typeof $('.cube-dialog-screen').get(0) != 'undefined') {
					return;
				};
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				//事件冒泡处理
				if(Util.eventBubble(el) == false){
					return;
				}
				
				this.navigate("tweet-detail?id=" + id, {
					trigger: true
				});

			},
			goToIssueTweet: function() {
				var from = "tweet-list";
				this.navigate("tweet-issue?from=" + from, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				bigImg = new BigImg();
				var reload = Util.request("reload") == 1 ? true : false;
				// console.info(reload);
				// tweet-tweet-list
				Util.loadList(this, 'tweet-tweet-list', OpenAPI.tweet_list, {
					'user': 0,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				}, reload);
				//绑定长按
				var me = this;
				me.longTouch(me);
			
			},
			longTouch: function(me) {
				var hammertime = $(me.el).find("#tweet-tweet-list").hammer();
				hammertime.on("hold", ".tweetList", function(ev) {
					$(this).unbind("click");
					me.deleteTweet(ev);
				});

			}
		}); //view define

	});