define(['text!project/software-detail.html', "../base/openapi", '../base/util','../base/login/login', '../base/bigImg/bigImg'],
	function(viewTemplate, OpenAPI, Util,Login, BigImg) {
		return Piece.View.extend({
			id: 'project-software-detail',
			bigImg: null,
			events: {
				"click .backBtn": "goBack",
				"click .collect": "collect",
				"click img": "bigImg"
			},
			bigImg:function(el) {
				Util.bigImg(el);
			},
			goBack: function() {
				history.back();
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			collect: function(el) {
				var id = $(el.currentTarget).attr("data-id");
				console.info(id);
				Util.checkLogin();
				var login = new Login();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login.show();
				} else {

					var noFavorite = $(".collect").hasClass("icon-star-empty");
					// 判断此用户是否以收藏此页面
					if (noFavorite) {
						var token = Piece.Store.loadObject("user_token");
						var user_message = Piece.Store.loadObject("user_message");
						var accesstoken = token.access_token;
						Util.Ajax(OpenAPI.favorite_add, "GET", {
							id: id,
							type: 1,
							access_token: accesstoken,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							var yesORno = data.error;
							console.info(yesORno + "====eeror")
							if (yesORno == 200) {
								new Piece.Toast('添加收藏成功');
								$('.collect').removeClass("icon-star-empty").addClass("icon-star");
								$('.icon-star').css("color", "white");
							} else {
								new Piece.Toast('添加收藏失败');
							}

						}, null);
					} else {
						var token = Piece.Store.loadObject("user_token");
						var user_message = Piece.Store.loadObject("user_message");
						var accesstoken = token.access_token;
						Util.Ajax(OpenAPI.favorite_remove, "GET", {
							id: id,
							type: 1,
							access_token: accesstoken,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							var yesORno = data.error;
							if (yesORno == 200) {
								new Piece.Toast('取消收藏成功');
								$('.collect').removeClass("icon-star").addClass("icon-star-empty");
								$('.collect').css("color", "white");

							} else {
								new Piece.Toast('取消收藏失败');
							}

						}, null);
					}
				}
			},
			onShow: function() {
				bigImg = new BigImg();
				var me = this;
				var url = Util.request("url");
				var urlSplitArr = url.split("/");
				var id = urlSplitArr[urlSplitArr.length - 1];
				var userToken = Piece.Store.loadObject("user_token");
				var options;
				if (userToken && userToken != null && userToken != "") {
					var accessToken = userToken.access_token;
					options = {
						'access_token': accessToken,
						"ident": id,
						"dataType": "jsonp"
					};
				} else {
					options = {
						"ident": id,
						"dataType": "jsonp"
					};
				}
				Util.Ajax(OpenAPI.project_detail, "GET", options, 'json', function(data, textStatus, jqXHR) {
					if (data != null) {
						var dataTemplate = data;
						dataTemplate.body = Util.removeImg(dataTemplate.body);
						var softwareDetailTemplate = $(me.el).find("#softwareDetailTemplate").html();
						var softwareDetailHtml = _.template(softwareDetailTemplate, dataTemplate);
						$(".content").html("");
						$(".content").append(softwareDetailHtml);
						$(".collect").attr("data-id", data.id);
						// if (data.favorite === 1) {
						// 	$('.collect').removeClass("icon-star-empty").addClass("icon-star");
						// 	$('.icon-star').css("color", "#0882f0");
						// }
						//判断是否登录了，如果没登陆那么都统一显示为未收藏，如果登录了，按情况显示
						var checkLogin = Util.checkLogin();
						if (checkLogin === true) {
							console.info(data);
							if (data.favorite === 1) {
								$('.collect').removeClass("icon-star-empty").addClass("icon-star");
								$('.icon-star').css("color", "white");
							}
						}

					} else {
						new Piece.Toast("没有相关数据");
					}

				}, null, null);
				//write your business logic here :)
			}
		}); //view define

	});