package net.oschina.cordova.sharesdk;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
import cn.sharesdk.demo.OneKeyShareCallback;
import cn.sharesdk.demo.ShareContentCustomizeDemo;
import cn.sharesdk.onekeyshare.OnekeyShare;

public class ShareSDKPlugin extends CordovaPlugin {
	private static final String TAG = "ShareSDKPlugin";
	private static final String ACTION_SHOW_SHARE_DIALOG = "showShareDialog";
	private Context mContext;

	@Override
	public void initialize(CordovaInterface cordova, CordovaWebView webView) {
		Log.v(TAG, "initialize");
		super.initialize(cordova, webView);
		mContext = cordova.getActivity();
	}

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		Log.v(TAG, "execute>>" + action + ",arg>>" + args.toString());
		JSONObject param = args.getJSONObject(0);
		if (param != null) {
			OnekeyShare oks = new OnekeyShare();
			// oks.setNotification(R.drawable.ic_launcher,
			// getString(R.string.app_name));
			String address = getJsonObject(param, "address");

			if (address != null) {
				oks.setAddress(address);
			}
			String title = getJsonObject(param, "title");
			if (title != null) {
				oks.setTitle(title);
			}
			String titleUrl = getJsonObject(param, "titleUrl");
			if (titleUrl != null) {
				oks.setTitleUrl(titleUrl);
			}
			String text = getJsonObject(param, "text");
			if (text != null) {
				oks.setText(text);
			}
			String imagePath = getJsonObject(param, "imagePath");
			if (imagePath != null) {
				oks.setImagePath(imagePath);
			}
			String imageUrl = getJsonObject(param, "imageUrl");
			if (imageUrl != null) {
				oks.setImageUrl(imageUrl);
			}
			String url = getJsonObject(param, "url");
			if (url != null) {
				oks.setUrl(url);
			}
			String filePath = getJsonObject(param, "filePath");
			if (filePath != null) {
				oks.setFilePath(filePath);
			}
			String comment = getJsonObject(param, "comment");
			if (comment != null) {
				oks.setComment(comment);
			}
			String site = getJsonObject(param, "site");
			if (site != null) {
				oks.setSite(site);
			}
			String siteUrl = getJsonObject(param, "siteUrl");
			oks.setSiteUrl(siteUrl);
			String venueName = getJsonObject(param, "venueName");
			if (venueName != null) {
				oks.setVenueName(venueName);
			}
			String venueDescription = getJsonObject(param, "venueDescription");
			if (venueDescription != null) {
				oks.setVenueDescription(venueDescription);
			}
			String latitude = getJsonObject(param, "latitude");
			String longitude = getJsonObject(param, "longitude");
			try {
				if (latitude != null) {
					oks.setLatitude(Float.parseFloat(latitude));
				}
				if (longitude != null) {
					oks.setLongitude(Float.parseFloat(longitude));
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			oks.setSilent(false);
			oks.setShareContentCustomizeCallback(new ShareContentCustomizeDemo());
			oks.setCallback(new OneKeyShareCallback(cordova.getActivity()));
			oks.show(cordova.getActivity());
		}

		callbackContext.success();
		return true;
	}

	private String getJsonObject(JSONObject jo, String key)
			throws JSONException {
		if (jo.has(key)) {
			return jo.getString(key);
		} else {
			return null;
		}
	}
}
