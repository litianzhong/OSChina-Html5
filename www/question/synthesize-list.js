define(['text!question/synthesize-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'question_synthesize-list',
			events: {
				"click .questionContent": "goToQuestionList",
				"click .editBtn": "editQuestion",
				"click .questionListImg":'goToUserInfor'
			},
			goToUserInfor:function(imgEl){
				Util.imgGoToUserInfor(imgEl);
			},
			goToQuestionList: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var from = $('.active').attr('data-value').split('/')[1];
				//checkDtail  toggle comment-list or xx-detail
				var checkDetail = "question/question-detail";
				//type    add  favorite
				var type = 2;
				//comment list
				var com = 2;
				this.navigate("question-detail?id=" + id + "&from=" + from + "&checkDetail=" + checkDetail + "&fromType=" + type+ "&com=" + com, {
					trigger: true
				});
			},
			editQuestion: function() {
				var from = $('.active').attr('data-value').split('/')[1];
				this.navigate("question-edit?type=3"+ "&from=" + from, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				Util.loadList(this, 'question-synthesize-list', OpenAPI.question_list, {
					'catalog': 3,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				});
			}
		}); //view define

	});